import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { Client } from '../models/Client';

@Injectable()
export class ClientService {
  // Properties
  clientsRef: AngularFireList<any>;
  clients: Observable<any[]>;
  client: Observable<any>;

  // To use the AngularFireDatabase it needs to be injected into the constructor
  constructor(private db: AngularFireDatabase) {
    // Does not fetch
    this.clientsRef = this.db.list('clients');

    // Fetch the whole firebase object, 'key:' included
    // 'key:' is needed for authentication
    this.clients = this.clientsRef.snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val()}));
    });
   }

   // Returns an Observable
   getClients() {
     return this.clients;
   }

   // Adds new clients to the database
   newClient(client: Client) {
     this.clientsRef.push(client);
   }
   // looks in the client database for a match with the id and returns that *client as an Observable
   getClient(id: string) {
     this.client = this.db.object('/clients/' + id).valueChanges();
     return this.client;
   }

   updateClient(id: string, client: Client) {
    return this.clientsRef.update(id, client);
   }

   deleteClient(id: string) {
     return this.clientsRef.remove(id);
   }
}
