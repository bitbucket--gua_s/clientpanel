import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { SettingsService } from '../services/settings.service';

@Injectable() export class RegisterGuard implements CanActivate {
    constructor(
        private r: Router,
        private settings: SettingsService
    ) {}

    canActivate(): boolean {
        if (this.settings.getSettings().allowRegistration) {
            return true;
        } else {
            this.r.navigate(['/login']);
            return false;
        }
    }
}
