import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { SettingsService } from '../../services/settings.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isLoggedIn: boolean;
  loggedInUser: string;
  showRegister: boolean;

  constructor(
    private authS: AuthService,
    private r: Router,
    private flash: FlashMessagesService,
    private settings: SettingsService
  ) { }

  ngOnInit() {
    this.authS.getAuth().subscribe(auth => {
      if (auth) {
        this.isLoggedIn = true;
        this.loggedInUser = auth.email;
      } else {
        this.isLoggedIn = false;
      }
    });
    // To allow registration via the settings
    this.showRegister = this.settings.getSettings().allowRegistration;
  }

  onLogoutClick() {
    this.authS.logout();
    this.flash.show('You are logged out', {
      cssClass: 'alert-success', timeout: 4000
    });
    this.r.navigate(['/']);
  }

}
