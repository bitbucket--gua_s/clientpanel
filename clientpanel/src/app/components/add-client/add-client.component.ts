import { Component, OnInit } from '@angular/core';
// Import Module
import { Client } from '../../models/Client';
// Import the Service
import { FlashMessagesService } from 'angular2-flash-messages';
// Import Router for redirecting
import { Router } from '@angular/router';
// Firebase hooks
import { ClientService } from '../../services/client.service';
import { SettingsService } from '../../services/settings.service';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css']
})
export class AddClientComponent implements OnInit {
  // Property with the type of the Model, *emtpy fields
  client: Client = {
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    balance: 0,
  };
  disableBalanceOnAdd: boolean;

// Inject it as a dependency
  constructor(
    private flashMessagesService: FlashMessagesService,
    private router: Router,
    private clientService: ClientService,
    private settingsService: SettingsService
  ) {}

  ngOnInit() {
    this.disableBalanceOnAdd = this.settingsService.getSettings().disableBalanceOnAdd;
  }

  // Object from the form
  onSubmit({value, valid}: {value: Client, valid: boolean}) {
    // console.log(valid, value);
    if (this.disableBalanceOnAdd) {
      value.balance = 0;
    }
    // If the form is not valid Flash a message
    if (!valid) {
      this.flashMessagesService.show('Please fill in all fields', {
        cssClass: 'alert-danger', timeout: 4000
      });
      this.router.navigate(['add-client']);
    } else {
      // Adds new client to the service aka Firebase
      this.clientService.newClient(value);
      this.flashMessagesService.show('New client added', {
        cssClass: 'alert-success', timeout: 4000 });
      this.router.navigate(['/']);
    }
  }

}
