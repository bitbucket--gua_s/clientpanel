import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../services/client.service';
import { Client } from '../../models/Client';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {
  clients: any[];
  totalOwed: number;
  // When a service is imported into a component it needs to be injected into the constructor.
  // By doing so we are able to use service and all its methods inside this component.
  constructor(private clientService: ClientService) { }

  ngOnInit() {
    // On init we want to subscribe to the Observable
    this.clientService.getClients().subscribe(clients => {
      // console.log(clients);
      // By setting the component array equal to the Observable it is usable on the template.
      this.clients = clients;
      // 2) To init the sum
      this.getTotalOwed();
    });
  }
  // 1) Sum of all 'balance' property inside the object
  getTotalOwed() {
    let total = 0;
    for (let i = 0; i < this.clients.length; i++) {
      total += parseFloat(this.clients[i].balance);
    }
    this.totalOwed = total;
  }

}
