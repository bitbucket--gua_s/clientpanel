// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  // Firebase Initialization
  firebase: {
    apiKey: 'AIzaSyDJvAmiu-iszORg3afsxdx9X7uk2P_OLHU',
    authDomain: 'clientpanel-7abe6.firebaseapp.com',
    databaseURL: 'https://clientpanel-7abe6.firebaseio.com',
    projectId: 'clientpanel-7abe6',
    storageBucket: 'clientpanel-7abe6.appspot.com',
    messagingSenderId: '227155939415'
  }
};
